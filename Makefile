.PHONY: build
build:
	@ mkdir -p ./bin
	go build -o bin/spritmonitorscraper .

.PHONY: test
test:
	@ go test ./...

.PHONY: vendor
vendor:
	go mod tidy
	go mod vendor
