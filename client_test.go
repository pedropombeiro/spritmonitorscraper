package spritmonitorscraper_test

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/pedropombeiro/spritmonitorscraper"
)

func TestLogin(t *testing.T) {
	phpsessid, err := spritmonitorscraper.Login(os.Getenv("SPRITMONITOR_USERNAME"), os.Getenv("SPRITMONITOR_PASSWORD"))
	assert.NoError(t, err)
	assert.NotEmpty(t, phpsessid)
}

func TestGetFuelingsCSV(t *testing.T) {
	phpsessid, err := spritmonitorscraper.Login(os.Getenv("SPRITMONITOR_USERNAME"), os.Getenv("SPRITMONITOR_PASSWORD"))
	require.NoError(t, err)
	require.NotEmpty(t, phpsessid)

	fuelings, err := spritmonitorscraper.GetFuelingsCSV(phpsessid, 739892)
	require.NoError(t, err)
	defer fuelings.Close()

	b, err := ioutil.ReadAll(fuelings)
	require.NoError(t, err)
	assert.NotEmpty(t, b)
}

func TestGetCostsCSV(t *testing.T) {
	phpsessid, err := spritmonitorscraper.Login(os.Getenv("SPRITMONITOR_USERNAME"), os.Getenv("SPRITMONITOR_PASSWORD"))
	require.NoError(t, err)
	require.NotEmpty(t, phpsessid)

	costs, err := spritmonitorscraper.GetCostsCSV(phpsessid, 739892)
	require.NoError(t, err)
	defer costs.Close()

	b, err := ioutil.ReadAll(costs)
	require.NoError(t, err)
	assert.NotEmpty(t, b)
}
