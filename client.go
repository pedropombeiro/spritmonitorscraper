package spritmonitorscraper

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"
)

var (
	c = http.Client{
		Timeout: 10 * time.Second,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	re = regexp.MustCompile(".*PHPSESSID=([0-9a-z]+);.*")
)

func Login(username, password string) (string, error) {
	v := url.Values{}
	v.Add("username", username)
	v.Add("password", password)
	v.Add("stayloggedin", "0")
	v.Add("login", "Login")
	resp, err := c.PostForm("https://www.spritmonitor.de/en/login.html", v)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusFound {
		return "", fmt.Errorf("login request failed: %s", resp.Status)
	}

	setCookie := resp.Header.Get("set-cookie")
	if !strings.Contains(setCookie, "PHPSESSID=") {
		return "", errors.New("login failed")
	}

	matches := re.FindStringSubmatch(setCookie)
	return matches[1], nil
}

func GetFuelingsCSV(phpsessid string, id int) (io.ReadCloser, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("https://www.spritmonitor.de/en/fuelings/%d/csvexport.csv", id), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("cookie", fmt.Sprintf("_sp_enable_dfp_personalized_ads=false; PHPSESSID=%s; id5id.1st_433_nb=16", phpsessid))
	resp, err := c.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("request failed: %s", resp.Status)
	}

	return resp.Body, nil
}

func GetCostsCSV(phpsessid string, id int) (io.ReadCloser, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("https://www.spritmonitor.de/en/costs_notes/%d/csvexport.csv", id), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("cookie", fmt.Sprintf("_sp_enable_dfp_personalized_ads=false; PHPSESSID=%s; id5id.1st_433_nb=16", phpsessid))
	resp, err := c.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("request failed: %s", resp.Status)
	}

	return resp.Body, nil
}
